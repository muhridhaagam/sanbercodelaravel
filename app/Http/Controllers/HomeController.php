<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
     public function index()
    {
        $data['title'] = "Home";
        return view('home', $data);
    }

       public function table()
    {
        $data['title'] = "Table";
        return view('table', $data);
    }

    public function data_table()
    {
        $data['title'] = "Data Table";
        return view('data-table', $data);
    }

}

@extends('layouts.master')
@section('content')

<section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->

               <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>Tables</h3>

                <p>Latihan 1</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-apps"></i>
              </div>
              <a href="/table" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
            </section>
          <section class="content">
   <div class="container-fluid">
              <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>Data Tables</h3>

                <p>Latihan 2</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="/data-tables" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
             </section>
          <!-- ./col -->


     


@endsection